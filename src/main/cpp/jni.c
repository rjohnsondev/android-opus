#include <jni.h>
#include <stdlib.h>
#include <stdio.h>
#include <opus.h>

typedef struct {
    OpusEncoder *encoder;
    OpusDecoder *decoder;
    int channels;
} opus_t;

JNIEXPORT void JNICALL
Java_com_gitlab_rjohnson_opusjni_Opus_open(JNIEnv *env, jobject thiz, jint channels,
                                           jint sampleFreq, jint brate) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");

    int err = -1;
    opus_t *opus = malloc(sizeof(opus_t));
    opus->encoder = opus_encoder_create(sampleFreq, channels, OPUS_APPLICATION_VOIP, &err);
    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }
    opus->decoder = opus_decoder_create(sampleFreq, channels, &err);
    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }

    opus->channels = channels;
    (*env)->SetLongField(env, thiz, fid, (jlong) opus);

    err = opus_encoder_ctl(opus->encoder, OPUS_SET_BITRATE(brate));
    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }

    err = opus_encoder_ctl(opus->encoder, OPUS_SET_INBAND_FEC(1));
    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }

    // 4% packet loss aught to be enough for anybody
    err = opus_encoder_ctl(opus->encoder, OPUS_SET_PACKET_LOSS_PERC(4));
    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }


    return;
}

JNIEXPORT int JNICALL
Java_com_gitlab_rjohnson_opusjni_Opus_encodeFrame(JNIEnv *env, jobject thiz,
                                                  jshortArray in_array, jint in_len_samples,
                                                  jbyteArray out_array) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t *) (*env)->GetLongField(env, thiz, fid);
    jint written = 0;

    int frame_size = in_len_samples / opus->channels;
    int out_len = (*env)->GetArrayLength(env, out_array);

    jshort *in_buffer_ptr = (*env)->GetShortArrayElements(env, in_array, NULL);
    jbyte *out_buffer_ptr = (*env)->GetByteArrayElements(env, out_array, NULL);
    written = opus_encode(opus->encoder, in_buffer_ptr, frame_size,
                          (unsigned char *) out_buffer_ptr, out_len);
    (*env)->ReleaseShortArrayElements(env, in_array, in_buffer_ptr, 0);
    (*env)->ReleaseByteArrayElements(env, out_array, out_buffer_ptr, 0);

    if (written < 0) {
        int err = written;
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return -1;
    }

    return written;
}


JNIEXPORT jint JNICALL
Java_com_gitlab_rjohnson_opusjni_Opus_decodeFrame(JNIEnv *env, jobject thiz,
                                                  jbyteArray in_array, jint in_len_bytes,
                                                  jshortArray out_array, jboolean fec) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t *) (*env)->GetLongField(env, thiz, fid);

    int out_len = (*env)->GetArrayLength(env, out_array);
    int frame_size = out_len / opus->channels;
    int decoded_samples = 0;

    if (in_array == NULL) {
        // packet loss concealment
        jshort *out_buffer_ptr = (*env)->GetShortArrayElements(env, out_array, NULL);
        decoded_samples = opus_decode(opus->decoder, NULL, in_len_bytes, out_buffer_ptr, frame_size, fec);
        (*env)->ReleaseShortArrayElements(env, out_array, out_buffer_ptr, 0);
    } else {
        jbyte *in_buffer_ptr = (*env)->GetByteArrayElements(env, in_array, NULL);
        jshort *out_buffer_ptr = (*env)->GetShortArrayElements(env, out_array, NULL);
        decoded_samples = opus_decode(opus->decoder, (const unsigned char *) in_buffer_ptr,
                                      in_len_bytes,
                                      out_buffer_ptr, frame_size, fec);
        (*env)->ReleaseByteArrayElements(env, in_array, in_buffer_ptr, 0);
        (*env)->ReleaseShortArrayElements(env, out_array, out_buffer_ptr, 0);
    }

    if (decoded_samples < 0) {
        int error = decoded_samples;
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(error));
        return -1;
    }

    return decoded_samples;
}


JNIEXPORT void JNICALL
Java_com_gitlab_rjohnson_opusjni_Opus_close(JNIEnv *env, jobject thiz) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t *) (*env)->GetLongField(env, thiz, fid);

    opus_encoder_destroy(opus->encoder);
    opus_decoder_destroy(opus->decoder);
    free(opus);

    (*env)->SetLongField(env, thiz, fid, 0);
    return;
}
