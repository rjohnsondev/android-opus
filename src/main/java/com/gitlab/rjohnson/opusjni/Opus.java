package com.gitlab.rjohnson.opusjni;

public class Opus implements AutoCloseable {
    static {
        if (Config.natives) {
            System.loadLibrary("opusjni");
        }
    }

    private long handle;

    public native void open(int channels, int sampleRate, int brate);

    public native int encodeFrame(short[] in_buf, int in_len_samples, byte[] out_buf);

    public native int decodeFrame(byte[] in_buf, int in_len_bytes, short[] out_buf, boolean fec);

    public native void close();

}
